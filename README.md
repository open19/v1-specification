# Open19 V1 Specification

This project contains the Open19 V1 specifications.  The latest version of the specification can be found under Repository --> Files. 

If you are interested in participating in the Open19 V2 Specification discussions, look at the RFC Discussions Working group. 

**All documentation associated with a project must be licened under either a Creative Commons Attribution lincense (CC-BY) or, in the case of technical specifications under a Creative Commons Attribition NoDerivs licese (CC-BY-ND)**

